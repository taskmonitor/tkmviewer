/* key-value-pair.h
 *
 * Copyright 2024 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define KEY_VALUE_PAIR_TYPE (key_value_pair_get_type ())
G_DECLARE_FINAL_TYPE (KeyValuePair, key_value_pair, KEY_VALUE, PAIR, GObject)

KeyValuePair *key_value_pair_new();
KeyValuePair *key_value_pair_new_with_data (const gchar *key, const gchar *val);
void key_value_pair_set_key (KeyValuePair *self, const gchar *key);
void key_value_pair_set_val (KeyValuePair *self, const gchar *val);

const gchar *key_value_pair_get_key(KeyValuePair *self);
const gchar *key_value_pair_get_val(KeyValuePair *self);

G_END_DECLS
