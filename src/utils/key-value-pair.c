/* key-value-pair.c
 *
 * Copyright 2024 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "key-value-pair.h"

enum {
  PROP_KEY = 1,
  PROP_VAL,
  N_PROPERTIES
};

static GParamSpec *key_value_pair_props[N_PROPERTIES] = { NULL };

/* Private structure definition. */
typedef struct _KeyValuePairPrivate {
  char *key;
  char *val;
} KeyValuePairPrivate;

struct _KeyValuePair {
  GObject parent;

  /* private */
  KeyValuePairPrivate *priv;
};

G_DEFINE_TYPE (KeyValuePair, key_value_pair, G_TYPE_OBJECT)

static void
key_value_pair_set_property (GObject      *object,
                             guint id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  KeyValuePair *self = KEY_VALUE_PAIR (object);

  switch (id)
    {
    case PROP_KEY:
      if (self->priv->key != NULL)
        g_free (self->priv->key);
      self->priv->key = g_strdup (g_value_get_string (value));
      break;

    case PROP_VAL:
      if (self->priv->val != NULL)
        g_free (self->priv->val);
      self->priv->val = g_strdup (g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, id, pspec);
      break;
    }
}

static void
key_value_pair_get_property (GObject      *object,
                             guint id,
                             GValue       *value,
                             GParamSpec   *pspec)
{
  KeyValuePair *self = KEY_VALUE_PAIR (object);

  switch (id)
    {
    case PROP_KEY:
      g_value_set_string (value, self->priv->key);
      break;

    case PROP_VAL:
      g_value_set_string (value, self->priv->val);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, id, pspec);
      break;
    }
}

static void
key_value_pair_finalize (GObject *gobject)
{
  KeyValuePair *self = KEY_VALUE_PAIR (gobject);

  g_free (self->priv->key);
  g_free (self->priv->val);
  g_free (self->priv);

  /* Always chain up to the parent class; as with dispose(), finalize()
   * is guaranteed to exist on the parent's class virtual function table
   */
  G_OBJECT_CLASS (key_value_pair_parent_class)->finalize (gobject);
}

KeyValuePair *
key_value_pair_new ()
{
  return g_object_new (KEY_VALUE_PAIR_TYPE, 0);
}

KeyValuePair *
key_value_pair_new_with_data (const gchar *key, const gchar *val)
{
  KeyValuePair *self = KEY_VALUE_PAIR (key_value_pair_new ());

  key_value_pair_set_key (self, key);
  key_value_pair_set_val (self, val);

  return self;
}

static void
key_value_pair_class_init (KeyValuePairClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = key_value_pair_set_property;
  object_class->get_property = key_value_pair_get_property;
  object_class->finalize = key_value_pair_finalize;

  key_value_pair_props[PROP_KEY] =
    g_param_spec_string ("key",
                         "Key",
                         "Key name",
                         NULL /* default value */,
                         G_PARAM_READWRITE);

  key_value_pair_props[PROP_VAL] =
    g_param_spec_string ("val",
                         "Value",
                         "Value for key",
                         NULL /* default value */,
                         G_PARAM_READWRITE);

  g_object_class_install_properties (object_class,
                                     N_PROPERTIES,
                                     key_value_pair_props);
}

static void
key_value_pair_init (KeyValuePair *self)
{
  g_return_if_fail (KEY_VALUE_IS_PAIR (self));
  self->priv = g_new0 (KeyValuePairPrivate, 1);
}

void
key_value_pair_set_key (KeyValuePair *self, const gchar *key)
{
  g_return_if_fail (KEY_VALUE_IS_PAIR (self));
  g_object_set (G_OBJECT (self), "key", key, NULL);
}

void
key_value_pair_set_val (KeyValuePair *self, const gchar *val)
{
  g_return_if_fail (KEY_VALUE_IS_PAIR (self));
  g_object_set (G_OBJECT (self), "val", val, NULL);
}

const gchar *
key_value_pair_get_key (KeyValuePair *self)
{
  g_return_val_if_fail (KEY_VALUE_IS_PAIR (self), NULL);
  return self->priv->key;
}

const gchar *
key_value_pair_get_val (KeyValuePair *self)
{
  g_return_val_if_fail (KEY_VALUE_IS_PAIR (self), NULL);
  return self->priv->val;
}
